import time
from pimoroni_i2c import PimoroniI2C
from breakout_sgp30 import BreakoutSGP30


def init_i2c(board_pins={"sda": 2, "scl": 3}):
    return PimoroniI2C(**board_pins)


def init_sensor(i2c):
    return BreakoutSGP30(i2c)


def start_measurement(sensor):
    print("SGP30 initialised - about to start measuring without waiting")
    sensor.start_measurement(False)
    id = sensor.get_unique_id()
    print(
        "Started measuring for id 0x",
        "{:04x}".format(id[0]),
        "{:04x}".format(id[1]),
        "{:04x}".format(id[2]),
        sep="",
    )


def reset_start(sensor):
    print("Resetting device")
    sensor.soft_reset()
    time.sleep(0.5)
    print("Restarting measurement, waiting 15 secs before returning")
    sensor.start_measurement(True)


def get_air_quality(sensor):
    air_quality = sensor.get_air_quality()
    eCO2 = air_quality[BreakoutSGP30.ECO2]
    TVOC = air_quality[BreakoutSGP30.TVOC]
    return eCO2, TVOC


def init_measurement(sensor):
    start_measurement(sensor)
    j = 0
    while True:
        j += 1
        eCO2, TVOC = get_air_quality(sensor)
        print(f"initialsing measurement in {30-j}")
        if j == 30:
            reset_start(sensor)
            break
        time.sleep(1.0)
    return eCO2, TVOC


def data_to_dic(data):
    if len(data) == 2:
        return {"eCO2": data[0], "TVOC": data[1]}


def run(PINS={"sda": 2, "scl": 3}):
    i2c = init_i2c(PINS)
    sensor = init_sensor(i2c)
    air_quality = init_measurement(sensor)
    return data_to_dic(air_quality)


if __name__ == "__main__":
    PINS = {"sda": 2, "scl": 3}
    i2c = init_i2c(PINS)
    sensor = init_sensor(i2c)
    air_quality = init_measurement(sensor)
    print(data_to_dic(air_quality))
