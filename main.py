# This example takes the temperature from the Pico's onboard temperature sensor, and displays it on Pico Display Pack, along with a little pixelly graph.
# It's based on the thermometer example in the "Getting Started with MicroPython on the Raspberry Pi Pico" book, which is a great read if you're a beginner!

import machine
import time
from pimoroni import RGBLED, Button
from picographics import PicoGraphics, DISPLAY_PICO_DISPLAY
import sgp30


# set up the hardware
display = PicoGraphics(display=DISPLAY_PICO_DISPLAY, rotate=0)
buttons = [Button(12), Button(13), Button(14), Button(15)]
led = RGBLED(6, 7, 8)

# set the display backlight to 50%
display.set_backlight(0.5)

# set up constants for drawing
WIDTH, HEIGHT = display.get_bounds()

BLACK = display.create_pen(0, 0, 0)
WHITE = display.create_pen(255, 255, 255)

conversion_factor = 3.3 / (
    65535
)  # used for calculating a temperature from the raw sensor reading

temp_min = 10
temp_max = 30
bar_width = 5

temperatures = []

colors = [(0, 0, 255), (0, 255, 0), (255, 255, 0), (255, 0, 0)]


def temperature_to_color(temp):
    temp = min(temp, temp_max)
    temp = max(temp, temp_min)

    f_index = float(temp - temp_min) / float(temp_max - temp_min)
    f_index *= len(colors) - 1
    index = int(f_index)

    if index == len(colors) - 1:
        return colors[index]

    blend_b = f_index - index
    blend_a = 1.0 - blend_b

    a = colors[index]
    b = colors[index + 1]

    return [int((a[i] * blend_a) + (b[i] * blend_b)) for i in range(3)]


led.set_rgb(0, 0, 255)
# Set LED to a converted HSV value

sgp30_i2c = sgp30.init_i2c()
sgp30_sensor = sgp30.init_sensor(sgp30_i2c)
sgp30.init_measurement(sgp30_sensor)


led.set_rgb(255, 255, 255)
# Set LED to a converted HSV value


def set_led_color(color, button_counter):
    if color == "green":
        if button_counter == 0:
            led.set_rgb(0, 255, 0)
        else:
            led.set_rgb(0, 0, 0)
    elif color == "yellow":
        if button_counter == 0:
            led.set_rgb(255, 255, 0)
        else:
            led.set_rgb(0, 0, 0)
    elif color == "orange":
        if button_counter == 0:
            led.set_rgb(100, 65, 0)
        else:
            led.set_rgb(5, 3, 0)
    elif color == "red":
        if button_counter == 0:
            led.set_rgb(255, 0, 0)
        else:
            led.set_rgb(10, 0, 0)


buttons_counter = 0
while True:
    # fills the screen with black
    display.set_pen(BLACK)
    display.clear()

    sgp30_data = sgp30.get_air_quality(sgp30_sensor)
    # print(sgp30_data)
    # the following two lines do some maths to convert the number from the temp sensor into celsius
    # draws a white background for the text
    display.set_pen(WHITE)
    display.rectangle(1, 1, 150, 25)

    # writes the reading as text in the white rectangle
    display.set_pen(BLACK)
    display.text(str(sgp30_data[0]) + "__eCO2", 3, 3, 0, 3)

    display.set_pen(WHITE)
    display.rectangle(1, 30, 150, 25)

    # writes the reading as text in the white rectangle
    display.set_pen(BLACK)
    display.text(str(sgp30_data[1]) + "__TVOC", 3, 33, 0, 3)

    # time to update the display
    display.update()

    if sgp30_data[0] < 800:
        set_led_color("green", buttons_counter)
    elif sgp30_data[0] >= 800 and sgp30_data[0] < 1100:
        set_led_color("yellow", buttons_counter)
    elif sgp30_data[0] >= 1100 and sgp30_data[0] < 1500:
        set_led_color("orange", buttons_counter)
    elif sgp30_data[0] >= 1500:
        set_led_color("red", buttons_counter)

    for button in buttons:
        if button.read():
            buttons_counter = (buttons_counter + 1) % 2

    time.sleep(0.1)
